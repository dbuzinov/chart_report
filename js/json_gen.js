var json = {
	id:56,//id:50,
	company_count:150,
	time_interval:120,
	hours:14,
	start_hour:8,
	start_minutes:30,
	start_date : '07-10-2014',
	end_date : '09-10-2014',
	start_time:'09:00',
	end_time:'22:00',
	interval:60,
	data:{},
	pie:{},
	pie1:{},
        engagement:{},
	api_url:'http://api.ultradev.mymbpanel.com/',//api_url:'http://api.alpha.mymbpanel.com/',//api_url:'http://api.ultradev.mymbpanel.com/',
	companies:{},
	sections:{},
	total_engagement:0,
	get_beacons:function(data){
		for(id in data){
			
				for(loc in data[id].locations){
					for(beacon in data[id].locations[loc].beacons){
						
							var b = data[id].locations[loc].beacons[beacon];
							json.companies[b.id] = b.title;
						
					}
				}
			
		}
	},
	get_sections:function(data){
		for(id in data){
			
				for(loc in data[id].locations){
					for(beacon in data[id].locations[loc].beacons){
						
							var b = data[id].locations[loc].beacons[beacon];
							json.sections[b.id] = data[id].locations[loc].title;
						
					}
				}
			
		}
	},
	get_main_chart:function(start_date,end_date,start_time,end_time,interval){
		var url = json.api_url + 'locationsfull/get/' + json.id +'/';
		$.ajax({
			crossOrigin: true,
			url: url,
			//type:'json',
	    	success: function(data) {
	    		data = JSON.parse(data);
	    		json.get_beacons(data);
	    		json.get_sections(data);
	    		var url = json.api_url + 'dwelltimemain/get/' + json.id +'/'+ start_date + '/' + end_date + '/' + interval + '/'+start_time+'/'+end_time;
		
				$.ajax({
					//async:false,
					crossOrigin: true,
					url: url,
					//type:'json',
			    	success: function(data) {
			    		var cur_date = json.formatDate(new Date());
			        	json.data = JSON.parse(data.replace(/(<pre>|<\/pre>)/g,'')).main_chart;
			        	
			        	for(date in json.data){
			        		
			        		if(date == cur_date){
			        			var data9 = json.data[date][json.start_time];
			        			for(time in json.data[date]){
			        				var t = new Date(cur_date + ' ' + time);
			        				var cur_t =  new Date();
			        				var start_t = new Date(cur_date + ' ' + json.start_time);
			        				if(t>cur_t && cur_t > start_t){
			        			
			        					delete json.data[date][time];
			        				}
			        				
			        			}
			        			
			        		}
			        		else {
			        			continue;
			        		}
			        	}
			        	
			        	json.update(json.formatDate(new Date()));
    
			        	
			        	json.buildDateSelector();
			        	json.buildCompaniesSelector();
			        	
			        	if(localStorage['selbox']){
					    	var arr = JSON.parse(localStorage['selbox']);
					    	$('#selbox').select2("val", arr);
					    	$('#select_date').change();
					    }
			        	
			      	}
				});
	    	}
			
		});
		
	},
	getConversion:function(json_data){
		var total = 0;
		var conversions = {};
		var arr = [];
		for(type in json_data){
			//if(type == 'promotions') continue;
			conversions[type] = 0;
			//conversions[type]['total_nonconverted'] = 0;
			
			for(t in json_data[type]){
				if(t == 'total_viewed'){
					conversions[type] += json_data[type][t];
					total += json_data[type][t];
				}
				
			
			}
		}
		
		conversions['non-converted'] = json.total_engagement - total;
		for (ctype in conversions){
			arr.push({
                    name: ctype,
                    y: conversions[ctype],
                    //sliced: true,
                    //selected: true
              });
		}
		return arr;
	},
	get_pie_chart:function(start_date,end_date){
		
		var url = json.api_url + 'engagement/get/' + json.id +'/'+ start_date + '/' + end_date; 
		$.ajax({
			//async:false,
			crossOrigin: true,
			url: url,
			type:'json',
	    	success: function(data) {
	    		data = JSON.parse(data);
	    		json.pie = data;
	        	
	        	var date = json.formatDate(new Date());
	        	
	        	var pie = new pie_chart('#browsGraph');
	        	pie.buildChart(json.formatPieJSON(data[date]));
                
                $('.redemption div p').text(json.total_engagement.toFixed(0));
	        	var conv = json.getTotalConversion(json.pie[date])/json.total_engagement*100;
	        	if(!conv){
	        			conv =0;
	        	}
	        	else {
	        		conv = conv.toFixed(2);
	        	}
	        	$('.conversion div p').html(conv + '<span> %</span>');
	      		
	      		/*var pie = new conversions_chart('#osGraph');
                pie.buildChart(json.pie[date]);*/
                
                var pie = new pie_chart('#osGraph');
	    		pie.buildChart(json.formatPieJSON2(json.pie[date]));
	      	}
		});
                
        var urlEngag = json.api_url + 'engagementnumber/get/' + json.id +'/'+ start_date + '/' + end_date; 
		$.ajax({
			//async:false,
			crossOrigin: true,
			url: urlEngag,
			type:'json',
	    	success: function(data) {
	    		data = JSON.parse(data);
                        json.engagement = data;
	        	var date = json.formatDate(new Date());
                json.total_engagement = data[date];
                
	      	}
		});
	},
        
	generate:function(){
		/*var company_count = json.company_count;
		var time_interval = json.time_interval;
		var hours = json.hours;
		var d = new Date();
		d.setHours(json.start_hour);
		d.setMinutes(json.start_minutes);
		
		var start_time = d;
		
		for(var i=1;i<=company_count;i++){
			
			json.companies.push('Company '+i);
		}
		
		
		
 		var day = 24*60*60*1000;
 		
 		for(var i=0;i<3;i++){
 			cur_date = new Date();
 			date_key = cur_date.getTime()-i*day;
 			date = new Date();
 			date.setTime(date_key);
 			var key = json.formatDate(date);
 			json.data[key] = {};
 			json.pie[key] = {};
 			for(var k=0;k<hours*60;k=k+time_interval){
 				var time = start_time.getTime()+k*60*1000;
 				var d = new Date(time);
 				var time_key = json.formatTime(d);
 				json.data[key][time_key]={};
 				for(company in json.companies){
 					json.data[key][time_key][json.companies[company]] = {count:(Math.random()*15).toFixed(0),avg:(Math.random()*10).toFixed(0)};
 					
 				}
 				
 			}
	 		
	 	}
 		*/
 		this.get_main_chart(this.start_date,this.end_date,this.start_time,this.end_time,this.interval);
 		this.get_pie_chart(this.start_date,this.end_date);
 		
 		
 		
 		//var pie = new pie_chart('#osGraph');
	    //pie.buildChart(json.generatePie());
 		
 		
 		
 		
 		//json.pie[key] = json.generatePie();
	 	//json.pie1[key] = json.generatePie();
 		
	},
	capitaliseFirstLetter:function(string)
	{
	    return string.charAt(0).toUpperCase() + string.slice(1);
	},
	getTotalConversion:function(json_data){
		var total = 0;
		for(type in json_data){
			for(t in json_data[type]){
				if(t == 'total_viewed'){
					total += json_data[type][t];
				}
			
			}
		}
		return total;
	},
	getTotal:function(json_data){
		var total = [];
		total['count'] = 0;
		total['avg'] = 0;
                total['beacon'] = 0;
                var companyArray = [];
		for(time in json_data){
			
			for(var company in json_data[time]){
                            
                                if(json_data[time][company].count > 0)
                                {
                                    companyArray.push(company);
                                }
				total['count'] += json_data[time][company].count;
				total['avg'] += json_data[time][company].avg;
			}
		}
                total['beacon'] = json.unique(companyArray).length;
                
		return total;
	},
        unique:function(arr)
        {
            var obj = {};
 
            for(var i=0; i<arr.length; i++) {
              var str = arr[i];
              obj[str] = true;
            }

            return Object.keys(obj);
        },
	formatPieJSON:function(data){
		 
		var pie = {};
		//json.total_engagement = 0;
		for(pie_data in data){
			pie_data_formated = json.capitaliseFirstLetter(pie_data);
			categories = [];
			categories_data = [];
			value = 0;
			for (category in data[pie_data]){
				var val = parseFloat((data[pie_data][category]).toFixed(2));
				categories.push(json.capitaliseFirstLetter(category.replace('_',' ')));
				categories_data.push(val);
				value += parseFloat(val);
			}
			pie[pie_data_formated] = {
				'categories': categories,
				data:categories_data,
				'value':value
			};
			total = 0;
			
			for(k in pie){
				total += pie[k].value;
			}
			
			//json.total_engagement += total;
			
			for(k in pie){
				pie[k].value = parseFloat((pie[k].value*100/total).toFixed(2));
				for(category in pie[k].data){
					pie[k].data[category] =  parseFloat((pie[k].data[category]*100/total).toFixed(2));
				}
			}
		}
		
	 	return pie;
	},
	formatPieJSON2:function(data){
		
		var pie = {};
		//json.total_engagement = 0;
		var conversions = json.getConversion(data);
		console.log();
		for (id in conversions){
			var a = conversions[id].y;
			var m = parseInt( a - Math.random()*a/2);
			var w = a - m;
			
			m = parseFloat((m*100/json.total_engagement).toFixed(2) );
			w = parseFloat((w*100/json.total_engagement).toFixed(2) );
			
			
			pie[conversions[id].name]={
	 				//name:'browser',
	 				//subname:'versions',
	 				categories: ['Male','Female'],
	 				data: [m,w],
	 				value:parseFloat((a*100/json.total_engagement).toFixed(2))
	 		};
	 	}
	 	return pie;
	},
	generatePie:function(){
		var left_pr = 100;
	 		var pie_data = [];
	 		
	 		for(j=1;j<=3;j++){
	 			
	 			pie_data.push(((100/4)*Math.random()).toFixed(2));
	 		}
	 		
	 		var sum = pie_data.reduce(function(previousValue, currentValue){
			    return currentValue + previousValue;
			});
			pie_data.push((100-parseFloat(sum)).toFixed(2));
	 		//console.log(pie_data);
	 		pie = {};
	 		var labels = ['Promotions','Polls','Articles','Surveys'];
	 		//var categories = ['Total viewed','Total Answer'];
	 		for(key1 in pie_data){
	 			var obj = {};
	 			var l,k;
	 			l=(0.7*parseFloat(pie_data[key1])).toFixed(2);
	 			k=(0.3*parseFloat(pie_data[key1])).toFixed(2);
	 			pie[labels[key1]]={
	 				//name:'browser',
	 				//subname:'versions',
	 				categories: ['Total viewed','Total Answer'],
	 				data: [parseFloat(l),parseFloat(k)],
	 				value:parseFloat(pie_data[key1])
	 			};
	 			
	 		}
	 		return pie;
	},
	formatDate:function(date){
		
		var date_formated = date.getFullYear() + '-' + ('0' + (date.getMonth()+1)).slice(-2) + '-' + ('0' + (date.getDate())).slice(-2);
		return date_formated;
	},
	formatTime:function(date){
		
		var date_formated = date.getHours() + '-' + date.getMinutes();
		return date_formated;
	},
	getTimeIntervals:function(data){
		var keys = [];
		for(var k in data) {
			keys.push(k);
		}
		
		return keys.sort();
	},
	preparePieChart:function(json_data){
		var colors = Highcharts.getOptions().colors;
		var data = [];
		var k=0;
		var pie = 0;
		for(pie in json_data){
			//var label = Object.keys(json_data[pie]);
			data.push({
				y: json_data[pie].value,
				color: colors[k],
				name:json_data[pie].name,
                drilldown: {
                	 name: json_data[pie].subname,
                     categories: json_data[pie].categories,
                     data: json_data[pie].data,
                     color: colors[k]
                }
            }
           );
           k++;
		}
		return data;
	},
	prepareTableDataTopHours:function(json_data,hours){
		var data = [];
		for(time in json_data){
			var last_keys = Object.keys(json_data).slice(-hours);
			
			for(company in json_data[time]){
				if(last_keys.indexOf(time) == -1){
					continue;
				}
				var comp_name = json.companies[company];
				if(!data[comp_name]){
					
					data[comp_name]={
                		count: 0,
                		avg: 0,
                		avg_t:0,
                		section:json.sections[company]
              		};
              	}		
              	
              	var count = json_data[time][company].count;
              	var avg = json_data[time][company].avg;
              	if(!count){
              		count = 0;
              	}
              	if(!avg){
              		avg = 0;
              	}
              	data[comp_name].avg += avg/60;
              	data[comp_name].avg_t += avg/60;
              	data[comp_name].count += count;
			}
		}
		//var time_arr = json.getTimeIntervals(json_data);
		for(key in data){
			avg = data[key].avg/hours;
			data[key].avg = !avg?0:avg;
		} 
		var chart_data = {};
		
		var top = json.getTop(json_data,10,hours);
		
		for(key in top){
			var dd = data[json.companies[top[key].name]];
			if(!dd){
				dd = {
					count: 0,
                	avg: 0,
                	avg_t:0,
                	section:json.sections[top[key].name]
				};
			}
			chart_data[json.companies[top[key].name]] = dd;
		}
		
		
		
		
		
		return chart_data;
	},
	
	prepareTableDataTopAll:function(json_data){
		var data = [];
		for(time in json_data){
			for(company in json_data[time]){
				var comp_name = json.companies[company];
				if(!data[comp_name]){
					
					data[comp_name]={
                		count: 0,
                		avg: 0,
                		avg_t:0,
                		section:json.sections[company]
              		};
              	}		
              	
              	var count = json_data[time][company].count;
              	var avg = json_data[time][company].avg;
              	if(!count){
              		count = 0;
              	}
              	if(!avg){
              		avg = 0;
              	}
              	data[comp_name].avg += avg/60;
              	data[comp_name].avg_t += avg/60;
              	data[comp_name].count = parseInt(data[comp_name].count) + parseInt(count);
			}
		}
		var time_arr = json.getTimeIntervals(json_data);
		for(key in data){
			avg = data[key].avg/time_arr.length;
			data[key].avg = !avg?0:avg;
		} 
		var chart_data = {};
		
		var top = json.getTop(json_data,10);
		
		for(key in top){
			chart_data[json.companies[top[key].name]] = data[json.companies[top[key].name]];
		}
		
		
		
		return chart_data;
	},
	prepareTableData:function(json_data){
		var data = [];
		for(time in json_data){
			for(company in json_data[time]){
				var comp_name = json.companies[company];
				if(!data[comp_name]){
					
					data[comp_name]={
                		count: 0,
                		avg: 0,
                		section:json.sections[company]
              		};
              	}		
              	
              	var count = json_data[time][company].count;
              	var avg = json_data[time][company].avg;
              	if(!count){
              		count = 0;
              	}
              	if(!avg){
              		avg = 0;
              	}
              	data[comp_name].avg += avg/60;
              	data[comp_name].count += count;
			}
		}
		var time_arr = json.getTimeIntervals(json_data);
		for(key in data){
			data[key].avg = data[key].count>0?data[key].avg/time_arr.length:0;
			
		}
		var chart_data = {};
		
		var top = json.getTop(json_data);
		if(json.getSelected(json_data) != false){
			var top = json.getSelected(json_data);
		}
		for(key in top){
			chart_data[json.companies[top[key].name]] = data[json.companies[top[key].name]];
		}
		return chart_data;
	},
	prepareChart:function(json_data,type){
		if(!type){
			type = 'count';
		}
		var data = [];
		var time_arr = json.getTimeIntervals(json_data);
		for(time in json_data){
			for(company in json_data[time]){
				
				if(!data[company]){
					var d = [];
					for(t in time_arr){
						d[time_arr[t]] = 0;
					}
					data[company]={
                		name: json.companies[company],
                		data: d,
                		
              		};
              	}		
              	
              	var v = json_data[time][company][type];
              	if(!v){
              		v = 0;
              	}
              	
              	data[company].data[time] = type=='avg'?v/60:v;
			}
		}
		
		for (k in data){
			var d = [];
			for (i in data[k].data){
				d.push(data[k].data[i]);
			}
			data[k].data = d;
		}
	
	
		
		var chart_data = [];
		var top = json.getTop(json_data);
		if(json.getSelected(json_data) != false){
			var top = json.getSelected(json_data);
		}
		for(key in top){
			chart_data.push(data[top[key].name]);
		}
		
		return chart_data;
	},
	compare:function (a,b) {
	  if (a.data < b.data)
	     return 1;
	  if (a.data > b.data)
	    return -1;
	  return 0;
	},
	getSelected:function(){
		var selected = $('#selbox').val();
		if(!selected){
			return false;
		}
		var top = [];
		for(key in  selected){
			top.push({name:selected[key]});
		}
		return top;
	},
	getTop:function(json_data,count,hours){
		if(!count){
			count = 10;
		}
		var last_keys;
		if(hours){
			last_keys = Object.keys(json_data).slice(-hours);
		}
		//var data = json.data;
		var now = json.formatDate(new Date);
		var top = [];
		var data = [];
		for(time in json_data){
			
			for(company in json_data[time]){
				if(hours && last_keys.indexOf(time) == -1){
					continue;
				}
				if(!data[company]){
					data[company]={
                		name: company,
                		data: 0,
                		
              		};
              	}
				
				
              	
              	data[company].data = parseInt(data[company].data) + parseInt(json_data[time][company].count);
			}
		}
		var chart_data = [];
		for(obj in data){
			chart_data.push(data[obj]);
		}
		chart_data.sort(json.compare);
		return chart_data.slice(0, count);;
	},
	setChartData:function (chart,date){
		i=0;
    	while(chart.series[i]){
    		chart.series[i].remove(false);
    	}
    	chart.redraw();
    	//chart.series=[];
    	var data = json.prepareChart(json.data[date]);
    	for(key in data){
    		chart.addSeries(data[key]);	
    	}
	},
	preperePieOptions:function(json_pie){
		var colors = Highcharts.getOptions().colors,
             categories = Object.keys(json_pie),
             data = json.preparePieChart(json_pie),
             browserData = [],
             versionsData = [],
             i,
             j,
             dataLen = data.length,
             drillDataLen,
             brightness;


         // Build the data arrays
         for (i = 0; i < dataLen; i += 1) {

             // add browser data
             browserData.push({
                 name: categories[i],
                 y: data[i].y,
                 color: data[i].color
             });

             // add version data
             drillDataLen = data[i].drilldown.data.length;
             for (j = 0; j < drillDataLen; j += 1) {
                 brightness = 0.2 - (j / drillDataLen) / 5;
                 versionsData.push({
                     name: data[i].drilldown.categories[j],
                     y: data[i].drilldown.data[j],
                     color: Highcharts.Color(data[i].color).brighten(brightness).get()
                 });
             }
         }
		var option = {
             chart: {
                 type: 'pie'
             },
             title: {
                 text: null
             },
             yAxis: {
                 title: {
                     text: 'Total percent market share'
                 }
             },
             plotOptions: {
                 pie: {
                     shadow: false,
                     center: ['50%', '50%']
                 }
             },
             tooltip: {
                 valueSuffix: '%'
             },
             series: [{
                 name: 'value',
                 data: browserData,
                 size: '60%',
                 dataLabels: {
                     formatter: function () {
                         return this.y > 5 ? this.point.name : null;
                     },
                     color: 'white',
                     distance: -30
                 }
             }, {
                 name: 'value',
                 data: versionsData,
                 size: '80%',
                 innerSize: '60%',
                 dataLabels: {
                     formatter: function () {
                         // display only if larger than 1
                         return this.y > 1 ? '<b>' + this.point.name + ':</b> ' + this.y + '%'  : null;
                     }
                 }
             }]
         };
         return option;
	},
	buildDateSelector:function(selector){
		if(!selector){
			selector = '#select_date';
		}
		for(key in json.data){
	    	var option = $(document.createElement('option'));
	    	option.html(key).val(key);
	    	$(selector).append(option);	
	    }
	    $(selector).val(json.formatDate(new Date()));
  },
  buildCompaniesSelector:function(selector){
  		if(!selector){
			selector = '#selbox';
		}
		for(key in json.companies){
	    	var option = $(document.createElement('option'));
	    	option.html(json.companies[key]).val(key);
	    	$(selector).append(option);	
	    }
	    $(selector).change(function(){
	    	$('#select_date').change();
	    });
  },
  update:function(date){
  		var json_data = json.data[date];
                json.total_engagement = json.engagement[date];
        
       	chart = new  os_chart();
       	chart.buildChart(json_data);
        
    	chart = new main_chart('#mainGrafCont','count','Count');
    	chart.buildChart(json_data);
        
        chartPer = new main_chart('#mainGrafContPercent');
        chartPer.buildChart(json_data,'avg','Avg Dwell Time');
	    
	    var table = new main_table();
	    table.buildTable(json_data);
	    
	    var table = new top_10_table('#top10_all','All');
	    table.buildTable(json_data);
	    
	    var table = new top_10_table('#top10_4',4);
	    table.buildTable(json_data);
	    
	    var table = new top_10_table('#top10_1',1);
	    table.buildTable(json_data);
	    
	    var total = json.getTotal(json_data);
            
    	$('.traffic div p').text(parseInt(total['count']));
    	
    	//var avgDwellTime = Math.round(Math.random()*60);
    	$('.time div p').html('0 <span> min</span>');
    	if(parseInt(total['count']) > 0){
    		$('.time div p').html(((total['avg']/60)/total['beacon']).toFixed(1) + '<span> min</span>');
    	}
    	//$('.time div p').html((total['avg']/60/total['count']).toFixed(1) + '<span> min</span>');
  }
};
	

