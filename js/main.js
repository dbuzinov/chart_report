

$(document).ready(function() {
    // Tabs
    json.generate();
    //$('#top10_all, #tableData, #top10_4, #top10_1').tablesorter();
    $('#select_date').change(function(){

    	//var chart = $('#mainGrafCont').highcharts();
        //var chartPer = $('#mainGrafContPercent').highcharts();
        
        json.update($(this).val());
        
        var pie = new pie_chart('#browsGraph');
	    pie.buildChart(json.formatPieJSON(json.pie[$(this).val()]));
	    
	    /*var pie = new conversions_chart('#osGraph');
        pie.buildChart(json.pie[$(this).val()]);
	    */
	    var pie = new pie_chart('#osGraph');
	    pie.buildChart(json.formatPieJSON2(json.pie[$(this).val()]));
	    
	    $('.redemption div p').text(json.total_engagement.toFixed(0));
	    
	    
	    //$('.conversion div p').html((json.getTotalConversion(json.pie[$(this).val()])/json.total_engagement*100).toFixed(2) + '<span> %</span>');
	        	   	
	    var conv = json.getTotalConversion(json.pie[$(this).val()])/json.total_engagement*100;
	        	if(!conv){
	        		conv =0;
	        	}
	        	else {
	        		conv = conv.toFixed(2);
	        	}
	        	
	        	$('.conversion div p').html(conv + '<span> %</span>');
    });
    
    
    
    chart_date = json.formatDate(new Date());
    $('.block_tabs_type_1 .tabs').tabs('.block_tabs_type_1 .tab_content', {
         initialIndex: 0
    });
    

    //Corection main chart size
    $('.block_tabs_type_1').click(function(){
            $('#mainGrafContPercent').highcharts().reflow();
            $('#mainGrafCont').highcharts().reflow();
    });
      	

        	
             
             
         
         
         
         // PIE charts
         
         
        

         /*var option = json.preperePieOptions(json.pie[chart_date]);
         $('#browsGraph').highcharts(option);

         var option = json.preperePieOptions(json.pie1[chart_date]);
         // Create the chart
         $('#osGraph').highcharts(option);
    */

$("#selbox").select2({
    maximumSelectionSize: 10,
    placeholder: "Select Companies",
    
}).on('change',function(){
	localStorage['selbox'] = JSON.stringify($(this).val());
});

    //Age Chart
    
    $(function () {
    var categories = [ '18-24', '24-30', '31-37', '38-43', '44-50', '50 + '];
    $(document).ready(function () {
        $('#ageChart').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                text: null
            },

            xAxis: [{
                categories: categories,
                reversed: false,
                labels: {
                    step: 1
                }
            }, { // mirror axis on right side
                opposite: true,
                reversed: false,
                categories: categories,
                linkedTo: 0,
                labels: {
                    step: 1
                }
            }],
            yAxis: {
                title: {
                    text: null
                },
                labels: {
                    formatter: function () {
                        return (Math.abs(this.value));
                    }
                },
                min: -60,
                max: 60
            },

            plotOptions: {
                series: {
                    stacking: 'normal'
                }
            },

            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + ', age ' + this.point.category + '</b><br/>' +
                        'Visitors: ' + Highcharts.numberFormat(Math.abs(this.point.y), 0);
                }
            },

            series: [{
                name: 'Male',
                data: [-28, -35, -43, -32, -15, -5]
            }, {
                name: 'Female',
                data: [20, 28, 38, 40, 9, 3]
            }]
        });
    });

});

        //Position chart
        
        
        $(function () {
    $('#positionChart').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: null
        },
        xAxis: {
            categories: ['Managers', 'Marketing Specialists', 'CEO', 'Engineers', 'Customers']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Amount of Visitors'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: 0,
            verticalAlign: 'top',
            y: 0,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
            }
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black, 0 0 3px black'
                    }
                }
            }
        },
        series: [{
            name: 'Male',
            data: [7, 5, 7, 9, 5]
        }, {
            name: 'Female',
            data: [3, 4, 4, 2, 3]
        }]
    });
});

    //OS Chart
    
    

    


    //Random num for top tabs
    
    //var avgDwellTime = Math.round(Math.random()*60);
    //var redemption = Math.round(Math.random()*1000);
    //var conversion = (Math.random()*100).toFixed(2);
    
    //$('.redemption div p').text(redemption);
    //$('.conversion div p').html(conversion + '<span> %</span>');
    //$('.time div p').html(avgDwellTime + '<span> min</span>');
    
});

setInterval(function(){
	
	json.generate();
	
},5*60*1000);