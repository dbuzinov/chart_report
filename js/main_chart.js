var conversions_chart = function conversions_chart(selector){
	this.selector=selector;
	if(!selector){
		this.selector='#osGraph';
	}
	this.setOptions = function(json_data){
		var options = {
        chart: {
            
            type:'pie',
            plotShadow: false
        },
        title: {
	            text: null
	    },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: false,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Conversion',
            data: json.getConversion(json_data)
        }]
    };
    return options;
	};
	this.buildChart=function(json_data){
		
		$(this.selector).highcharts(this.setOptions(json_data));
		//$(this.selector).highcharts();
		//var chart = $(this.selector).highcharts().reflow();

	};
	
};


var os_chart = function os_chart(selector){
	this.selector=selector;
	if(!selector){
		this.selector='#osChart';
	}
	this.setOptions = function(json_data){
		var categories = [];
		var count = 2000;
		var ios = [];
		var android = [];
		for(time in json_data){
			categories.push(time);
			a = parseInt(400*Math.random());
			b = 800+a;
			ios.push(b);
			android.push(count-b);
		}
		var options = {
	        chart: {
	            type: 'area'
	        },
	        title: {
	            text: null
	        },
	        
	        xAxis: {
	            categories: categories,
	            tickmarkPlacement: 'on',
	            title: {
	                enabled: false
	            }
	        },
	        yAxis: {
	            title: {
	                text: 'Percent'
	            }
	        },
	        tooltip: {
	            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.percentage:.1f}%</b><br/>',
	            shared: true
	        },
	        plotOptions: {
	            area: {
	                stacking: 'percent',
	                lineColor: '#ffffff',
	                lineWidth: 1,
	                marker: {
	                    lineWidth: 1,
	                    lineColor: '#ffffff'
	                }
	            }
	        },
	        series: [{
	            name: 'iOS',
	            data: ios
	        }, {
	            name: 'Android',
	            data: android
	        }]
        };
        return options;
	};
	this.buildChart=function(json_data,type,ytitle){
		
		$(this.selector).highcharts(this.setOptions(json_data));
		//$(this.selector).highcharts();
		//var chart = $(this.selector).highcharts().reflow();

	};
	
};



var main_chart  = function main_chart(selector,type,ytitle){
	this.selector=selector;
	if(!selector){
		this.selector='#mainGrafCont';
	}
	
	if(!ytitle){
		ytitle = 'Count';
	}
	this.setOptions = function(json_data,type,ytitle){
		var options = {
                  chart: {
                       type: 'spline',
                       zoomType: 'x'
                  },
                  title: {
                       text: null
                  },
                  xAxis: {
                       categories: json.getTimeIntervals(json_data)
                  },
                  yAxis: {
                        title: {
                             text: ytitle
                        },
                        min : 0
                  },
                  tooltip: {
                        pointFormat: '<span style="color:{series.color}">{series.name}</span>: {point.y:,.0f}<br/>',
                        shared: true
                  },
                  series: json.prepareChart(json_data,type)
             };
        return options;
	};
	this.buildChart=function(json_data,type,ytitle){
		
		$(this.selector).highcharts(this.setOptions(json_data,type,ytitle));
		//$(this.selector).highcharts();
		//var chart = $(this.selector).highcharts().reflow();

	};
};
var pie_chart = function pie_chart(selector) {
	if(!selector){
		selector = '#browsGraph';
	}
	this.buildChart = function(json_data){
		var option = json.preperePieOptions(json_data);
        $(this.selector).highcharts(option);
	};
	this.selector = selector;
};

var main_table = function main_table(selector){
	if(!selector){
		selector = '#tableData';
	}
	this.buildTable = function(json_data){
		var tr = $('<tr/>');
		var table_data = json.prepareTableData(json_data);
		$(selector).find('tbody').html('');
		for(company in table_data){
			
			tr.append($('<td/>').html(table_data[company].section));
			tr.append($('<td/>').html(company));
			tr.append($('<td/>').html(table_data[company].count));
			tr.append($('<td/>').html(table_data[company].avg.toFixed(1)+' min'));
			
			$(selector).find('tbody').append(tr);
			tr = $('<tr/>');
		}
		$(selector)
		 .unbind('appendCache applyWidgetId applyWidgets sorton update updateCell')
		 .removeClass('tablesorter')
		 .find('thead th')
		 .unbind('click mousedown')
		 .removeClass('header headerSortDown headerSortUp');
		 $(selector).tablesorter();
		
	};
};

var top_10_table = function top_10_table(selector,hours){
	if(!selector){
		selector = '#top10_all';
	}
	if(!hours){
		hours = 'All';
	}
	this.buildTable = function(json_data){
		var tr = $('<tr/>');
		if(hours !='All'){
			var table_data = json.prepareTableDataTopHours(json_data,hours);
		}
		else {
			var table_data = json.prepareTableDataTopAll(json_data);
		}
		$(selector).find('tbody').html('');
		var k = 1;
		for(company in table_data){
			tr.append($('<td/>').html(k));
			
			tr.append($('<td/>').html(company));
			tr.append($('<td/>').html(table_data[company].count));
			tr.append($('<td/>').html(table_data[company].avg_t.toFixed(1)));
			tr.append($('<td/>').html(table_data[company].avg.toFixed(1)));
			
			$(selector).find('tbody').append(tr);
			tr = $('<tr/>');
			k++;
		}
		$(selector)
		 .unbind('appendCache applyWidgetId applyWidgets sorton update updateCell')
		 .removeClass('tablesorter')
		 .find('thead th')
		 .unbind('click mousedown')
		 .removeClass('header headerSortDown headerSortUp');
		 $(selector).tablesorter();
	};
};


/*ar pie_chart  = {
	selector:'#browsGraph',
	buildChart:function(json_data){
		
		//$(this.selector).highcharts(this.setOptions(json_data));
	}
};
*/